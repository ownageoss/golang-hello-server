ARG PROGRAM=golang-hello-server

################################
# STEP 1 build executable binary
################################

# Debian 12 with go 1.24.1
FROM golang:1.24.1 AS build

ARG PROGRAM

WORKDIR /go/src/${PROGRAM}/
COPY . /go/src/${PROGRAM}/

# Build binary
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o /go/bin/${PROGRAM}

###############################
# STEP 2 deploy a smaller image
###############################

FROM gcr.io/distroless/static:latest

USER nonroot:nonroot

ARG PROGRAM

COPY --from=build /go/bin/${PROGRAM} /

ENTRYPOINT ["/golang-hello-server"]