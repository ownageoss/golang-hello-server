# Golang Hello Server

A simple, secure and light weight [gorilla mux](https://github.com/gorilla/mux) based HTTP server.

Will be actively maintained with Go releases.

## Objective

The objective of this repo is to demo:

- best practices for golang containers.
- best practices for deploying containers on kubernetes.

This container can also be used as a test container similar to [Docker's hello-world](https://hub.docker.com/_/hello-world) container.

## Run on local machine

Run locally on port `8000` as follows:

```
docker run --rm -p 8000:8000 registry.gitlab.com/ownageoss/golang-hello-server:latest
```

Then access on web browser:

```
http://localhost:8000
http://localhost:8000/health
```

To run on a different port, example `5000`:

```
docker run --rm -p 5000:8000 registry.gitlab.com/ownageoss/golang-hello-server:latest
```

## Run on Kubernetes

Create a kubernetes namespace

```
kubectl create ns hello
```

Then create a file called `hello.yaml` with the following contents:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  selector:
    matchLabels:
      app: hello
  template:
    metadata:
      labels:
        app: hello
    spec:
      securityContext:
        runAsNonRoot: true
        runAsUser: 65532
        runAsGroup: 65532
      terminationGracePeriodSeconds: 60
      containers:
        - name: hello
          securityContext:
            readOnlyRootFilesystem: true
          command: ["/golang-hello-server"]
          image: registry.gitlab.com/ownageoss/golang-hello-server:latest
          imagePullPolicy: Always
          ports:
            - containerPort: 5050
              protocol: TCP
          env:
            - name: HELLO_SERVER_PORT
              value: "5050"
          readinessProbe:
            httpGet:
              path: /health
              port: 5050
            initialDelaySeconds: 5
            periodSeconds: 10
          livenessProbe:
            httpGet:
              path: /health
              port: 5050
            initialDelaySeconds: 15
            periodSeconds: 20
          resources:
            requests:
              memory: "16Mi"
              cpu: "10m"
            limits:
              memory: "16Mi"
              cpu: "20m"
---
apiVersion: v1
kind: Service
metadata:
  name: hello
spec:
  type: NodePort
  selector:
    app: hello
  ports:
    - name: http
      protocol: TCP
      port: 5050
      targetPort: 5050
```

Apply as follows:

```
kubectl apply -f hello.yaml -n hello
```

Finally, setup an ingress to access your hello server.

## Software Bill of Materials (SBOM)

```
               _      
 ___ _ __   __| |_  __
/ __| '_ \ / _` \ \/ /
\__ \ |_) | (_| |>  < 
|___/ .__/ \__,_/_/\_\
    |_|               

 📂 SPDX Document gitlab.com/ownageoss/golang-hello-server
  │ 
  │ 📦 DESCRIBES 1 Packages
  │ 
  ├ golang-hello-server
  │  │ 🔗 14 Relationships
  │  ├ CONTAINS FILE .dockerignore (.dockerignore)
  │  ├ CONTAINS FILE Dockerfile (Dockerfile)
  │  ├ CONTAINS FILE .gitlab-ci.yml (.gitlab-ci.yml)
  │  ├ CONTAINS FILE Makefile (Makefile)
  │  ├ CONTAINS FILE .gitignore (.gitignore)
  │  ├ CONTAINS FILE go.sum (go.sum)
  │  ├ CONTAINS FILE go.mod (go.mod)
  │  ├ CONTAINS FILE LICENSE (LICENSE)
  │  ├ CONTAINS FILE README.md (README.md)
  │  ├ CONTAINS FILE golang-hello-server.png (golang-hello-server.png)
  │  ├ CONTAINS FILE hello.yaml (hello.yaml)
  │  ├ CONTAINS FILE main.go (main.go)
  │  ├ CONTAINS FILE golang-hello-server.spdx (golang-hello-server.spdx)
  │  └ DEPENDS_ON PACKAGE github.com/gorilla/mux@v1.8.1
  │ 
  └ 📄 DESCRIBES 0 Files
```