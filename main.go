package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
)

func HelloHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("Hello!\n"))
}

func HealthCheckHandler(w http.ResponseWriter, _ *http.Request) {
	// A very simple health check.
	_, _ = w.Write([]byte("Still alive!\n"))
}

func main() {
	var wait time.Duration
	flag.DurationVar(
		&wait,
		"graceful-timeout",
		time.Second*15,
		"the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m",
	)
	flag.Parse()

	r := mux.NewRouter()
	// Add your routes as needed

	r.HandleFunc("/", HelloHandler)
	r.HandleFunc("/health", HealthCheckHandler)

	port := os.Getenv("HELLO_SERVER_PORT")
	if port == "" {
		port = "8000"
	}

	srv := &http.Server{
		Addr: fmt.Sprintf(":%s", port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout:      time.Second * 15,
		ReadTimeout:       time.Second * 15,
		ReadHeaderTimeout: time.Second * 15,
		IdleTimeout:       time.Second * 60,
		Handler:           r, // Pass our instance of gorilla/mux in.
	}

	log.Printf("Starting HTTP server on port %s ...\n", port)

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Printf("\n%v\n", err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	_ = srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("Shutting down ...")

	os.Exit(0)
}
