GROUP=ownageoss
PROJECT_NAME=golang-hello-server
.DEFAULT_GOAL=run

run:
	go clean
	go build -o main -race
	./main

clean:
	-go clean

updatedeps:
	go get -u ./...
	go mod tidy

sbom:
	go clean
	bom generate --name gitlab.com/ownageoss/$(PROJECT_NAME) --output=$(PROJECT_NAME).spdx .
	bom document outline $(PROJECT_NAME).spdx

checks:
	gitleaks detect -v --no-git
	golangci-lint run --no-config
	govulncheck ./...
	gosec -tests ./...

brutal:
	golangci-lint run --enable-all